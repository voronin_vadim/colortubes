Native Share Plugin for iOS & Android by Parta Games

#### Unity3D iOS & Android project requirements and settings ####

* Unity version 5.4.3p4 or later
* Minimum Android SDK level: 9 (2.3 Gingerbread)
* Target Android SDK level: 23 (6.0 Marhsmallow)
* Minimum iOS version: 8.0
* Target iOS version: 11.0

#### Getting Started ####

1) Import all the files from NativeShare.unitypackage or the Asset Store
2) Go to "Build Settings" to make sure platform is Android or iOS
3) Make sure platform specific settings are set accordingly (look below for instructions)
4) Create a C# script and call NativeShare.Share(...) methods according to your UI flow
5) Build and enjoy

## iOS specific settings ##
1) In "Build Settings" change "Target Minimum iOS version" to 8.0
2) After building go to the Xcode project and add "nativeshare_ios.framework" to "Embedded Binaries" in target's "General" tab

## Android specific settings ##
1) If you have Android SDK 24+ installed, Unity will use it as the target SDK. In this case you might have problems using the "file:///" pattern to share files.
   Make sure your Plugins/Android/AndroidManifest.xml file contains: <uses-sdk android:minSdkVersion="9" android:targetSdkVersion="23" />
   Correct settings can be found inside the AndroidManifest-template.xml that comes with this plugin.

Copyright 2017 Parta Games Oy
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor; 

[CustomEditor(typeof (AdManager))]
public class AdManagerEditor : Editor {

    public override  void OnInspectorGUI()
    {

       
        EditorGUILayout.BeginHorizontal();
#if TEST_VERSION
        EditorGUILayout.HelpBox("TEST_VERSION selected",MessageType.Info);
#else
        EditorGUILayout.HelpBox("If test build you mast USE derective #TEST_VERSION", MessageType.Warning);
#endif
        EditorGUILayout.EndHorizontal(); 
        EditorGUILayout.Space();
         base.OnInspectorGUI();

    }
}

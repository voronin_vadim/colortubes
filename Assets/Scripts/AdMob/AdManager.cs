﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.SceneManagement;
using System;

public class AdManager : MonoBehaviour
{

    /// <summary>
    /// If final build you mast USE derective #FINAL_VERSION
    /// </summary>
    /// 
    float deltaTime;


    // -------------------- id -----------------
#if UNITY_ANDROID
    string appId = "ca-app-pub-6890259081888597~6350656635";
#elif UNITY_IPHONE
            string appId = "ca-app-pub-6890259081888597~9688455789";
#else
    string appId = "unexpected_platform";
#endif


#if UNITY_ANDROID && TEST_VERSION
    
    string bannerId = "ca-app-pub-3940256099942544/6300978111"; //  GP banner test
    string internalId = "ca-app-pub-3940256099942544/1033173712";  //GP internal test
    string rewardId = "ca-app-pub-3940256099942544/5224354917"; //Gp reward test

#elif UNITY_ANDROID && !TEST_VERSION

     string bannerId = "ca-app-pub-6890259081888597/7472166613";
     string internalId = "ca-app-pub-6890259081888597/3532921601";
     string rewardId = "ca-app-pub-6890259081888597/5916731733";

#elif UNITY_IPHONE && TEST_VERSION
  
    string bannerId = "ca-app-pub-3940256099942544/2934735716"; //  Ios banner test
    string internalId = "ca-app-pub-3940256099942544/4411468910";  //Ios internal test
    string rewardId = "ca-app-pub-3940256099942544/1712485313"; //Ios reward test

#elif UNITY_IPHONE && !TEST_VERSION
    
    string bannerId = "ca-app-pub-6890259081888597/5557639085"; 
    string internalId = "ca-app-pub-6890259081888597/2739904053"; 
    string rewardId = "ca-app-pub-6890259081888597/5174495707";
#else
    string bannerId = ""; 
    string internalId = ""; 
    string rewardId = "";
#endif



    //All ad mob ADS
    private static BannerView bannerView;
    private InterstitialAd interstitial;
    private static RewardBasedVideoAd rewardBasedVideo;
    private static string outputMessage = string.Empty;


    private void Start()
    {
        MobileAds.Initialize(appId);
        CreateRequest();
        InitAdParam();
    }

    void InitAdParam()
    {
       
        RequestInterstitial();
        RequestRewardBasedVideo();
        AdShowManager.Instance.CompleteInitAd();
    }


    AdRequest CreateRequest()
    {
        return new AdRequest.Builder().Build();
    }


    /// <summary>
    /// Show banner
    /// </summary>

    public void ShowBanner()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }
        string id = bannerId; 
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(id, AdSize.SmartBanner, AdPosition.Bottom);

        bannerView.OnAdLoaded += (senler, argument) => Debug.Log("Banner  Loaded");
        bannerView.OnAdFailedToLoad += (senler, argument) => Debug.Log("Banner  Fail Load");
        bannerView.OnAdOpening += (senler, argument) => Debug.Log("Banner  Opening");
        bannerView.OnAdClosed += (senler, argument) => Debug.Log("Banner Close");
        bannerView.OnAdLeavingApplication += (senler, argument) => Debug.Log("Banner  CloseApply");

        // Load a banner ad.
        bannerView.LoadAd(CreateRequest());
    }
    public void DestroyBanner()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }

    }


    /// <summary>
    /// Reward Request
    /// </summary>
    void RequestRewardBasedVideo()
    {
        if (rewardBasedVideo != null) return;
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        // RewardBasedVideoAd is a singleton, so handlers should only be registered once.
        rewardBasedVideo.OnAdLoaded += (senler, argument) => Debug.Log("Reward video Loaded");
        rewardBasedVideo.OnAdFailedToLoad += (senler, argument) => Debug.Log("Reward video  Fail on Load");
        rewardBasedVideo.OnAdOpening += (senler, argument) => Debug.Log("On  Reward AdOpening");
        rewardBasedVideo.OnAdStarted += (senler, argument) => Debug.Log("Reward video Started");
        rewardBasedVideo.OnAdRewarded += (senler, argument) => { AdShowManager.Instance.GetReward(); LoadReward(); };
        rewardBasedVideo.OnAdClosed += (senler, argument) => LoadReward();
        rewardBasedVideo.OnAdLeavingApplication += (senler, argument) => Debug.Log("Reward video leaving aplication");
        LoadReward();
    }
    public void LoadReward()
    {
        string id = rewardId ;
        rewardBasedVideo.LoadAd(CreateRequest(), id);

    }
    public void ShowRewardBasedVideo()
    {
        if (rewardBasedVideo.IsLoaded()) rewardBasedVideo.Show();
        else
        {
            MonoBehaviour.print("Reward based video ad is not ready yet");
            LoadReward();
        }

    }


    /// <summary>
    /// Internal Request
    /// </summary>
    void RequestInterstitial()
    {
        string id =  internalId;
        interstitial = new InterstitialAd(id);

        interstitial.OnAdLoaded += (senler, argument) => Debug.Log("INternal Loaded");
        interstitial.OnAdFailedToLoad += (senler, argument) => Debug.Log("INternal Loaded Fale");
        interstitial.OnAdOpening += (senler, argument) => Debug.Log("INternal ad Opened");
        interstitial.OnAdClosed += HandleOnAdClosed;
        this.interstitial.OnAdLeavingApplication += (senler, argument) => Debug.Log("INternal LivingUply");
        this.interstitial.LoadAd(CreateRequest());
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
        AdShowManager.Instance.InternalComplete();
        LoadInternal();
    }

    public void LoadInternal()
    {
        this.interstitial.LoadAd(CreateRequest());
    }
    public bool InternalLoaded()
    {
        return this.interstitial.IsLoaded();
    }
    public void ShowInternal()
    {
        if (this.interstitial.IsLoaded()) this.interstitial.Show();
        else MonoBehaviour.print("Interstitial is not ready yet");
    }



    void OnDestroy()
    {

        if (bannerView != null)
        {
            bannerView.Destroy();
        }
        if (interstitial != null)
        {
            interstitial.Destroy();
        }
    }



    #region ADD GUI
    /*
    public void OnGUI()
    {
        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, Screen.width, Screen.height);
        style.alignment = TextAnchor.LowerRight;
        style.fontSize = (int)(Screen.height * 0.06);
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float fps = 1.0f / this.deltaTime;
        string text = string.Format("{0:0.} fps", fps);
        GUI.Label(rect, text, style);

        // Puts some basic buttons onto the screen.
        GUI.skin.button.fontSize = (int)(0.035f * Screen.width);
        float buttonWidth = 0.35f * Screen.width;
        float buttonHeight = 0.15f * Screen.height;
        float columnOnePosition = 0.1f * Screen.width;
        float columnTwoPosition = 0.55f * Screen.width;

        Rect requestBannerRect = new Rect(
            columnOnePosition,
            0.05f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestBannerRect, "Request\nBanner"))
        {
            this.RequestBanner();
        }

        Rect destroyBannerRect = new Rect(
            columnOnePosition,
            0.225f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(destroyBannerRect, "Destroy\nBanner"))
        {
            this.bannerView.Destroy();
        }

        Rect requestInterstitialRect = new Rect(
            columnOnePosition,
            0.4f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestInterstitialRect, "Request\nInterstitial"))
        {
            this.RequestInterstitial();
        }

        Rect showInterstitialRect = new Rect(
            columnOnePosition,
            0.575f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(showInterstitialRect, "Show\nInterstitial"))
        {
            this.ShowInternal();
        }

        Rect destroyInterstitialRect = new Rect(
            columnOnePosition,
            0.75f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(destroyInterstitialRect, "Destroy\nInterstitial"))
        {
            this.interstitial.Destroy();
        }

        Rect requestRewardedRect = new Rect(
            columnTwoPosition,
            0.05f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(requestRewardedRect, "Request\nRewarded Video"))
        {
            this.RequestRewardBasedVideo();
        }



        Rect loaddRewardedRect = new Rect(
            columnTwoPosition,
            0.55f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(loaddRewardedRect, "Load\nRewarded Video"))
        {
            this.LoadReward();
        }



       

        Rect showRewardedRect = new Rect(
            columnTwoPosition,
            0.225f * Screen.height,
            buttonWidth,
            buttonHeight);
        if (GUI.Button(showRewardedRect, "Show\nRewarded Video"))
        {
            this.ShowRewardBasedVideo();
        }

        Rect textOutputRect = new Rect(
            columnTwoPosition,
            0.925f * Screen.height,
            buttonWidth,
            0.05f * Screen.height);
        GUI.Label(textOutputRect, outputMessage);
    }
    
    */
    #endregion




}


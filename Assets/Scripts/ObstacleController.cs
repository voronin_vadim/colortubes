﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq; 
     

public class ObstacleController : MonoBehaviour {

    public List<Rigidbody> obstacle;
    public List<Transform> startPosition;

     public float timerGeneratedStart;
    public float obstacleSpeed; 
     float timerGenerated;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        timerGenerated -= Time.deltaTime;

        if (timerGenerated <= 0)
        {
            int tripleBarNb = Random.Range(0, 6);
            GeneratedLine();
            timerGenerated =timerGeneratedStart;
        }
    }

    void GeneratedLine()
    {
        startPosition = startPosition.OrderBy(p => Random.value).ToList();
        for (int i = 0; i < obstacle.Count; i++)
        {
         var obs =    Instantiate(obstacle[i], startPosition[i].position, startPosition[i].rotation);
            obs.velocity = Vector3.down * obstacleSpeed; 
        }
    }
}

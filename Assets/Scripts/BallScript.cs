﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour {

	public ParticleSystem flashMid, flashLeft, flashRight,trail,explosionSys;

	private string currentColor="red";

	private string[] colorToChooseArray;

	private Color transparentBall;

	

	public AudioSource explosionSrc, flashSrc;

	private float volume;

    ColoringBall coloring;

    public float mVal = 0.03f;

    void Start()
    {

        coloring = GetComponent<ColoringBall>();

        //populate the colorToChooseArray with the 3 possibilities
        colorToChooseArray = new string[] { "blue", "green", "red" };

        //make transparentBall color transparent (this color will be used to hide the ball when it is destroyed)
        transparentBall.a = 0;

        //get the volume saved from playerprefs file and apply to volume to audio sources
        volume = PlayerPrefs.GetFloat("volume", 1f);
        explosionSrc.volume = volume;
        flashSrc.volume = volume;

        GMScript.instance.onContinue += Continue; 

    }

	//this is the function called by the right arrow button the make the ball move to the right
	public void MoveRight()
	{
		//creat the vector with which the ball will be moved
		Vector3 moveRightVector = new Vector3 (mVal, 0f, 0f);

		//check if the ball is not in the right tube
		if (transform.position.x < mVal) 
		{
			//play the flash particle systems according to the ball position
			if (transform.position.x == 0) 
			{
				flashMid.Play ();
			}
			if (transform.position.x == mVal) 
			{
				flashRight.Play ();
			}
			if (transform.position.x == -mVal) 
			{
				flashLeft.Play ();
			}

			//translate the ball and play the flash sound
			transform.Translate (moveRightVector, Space.World);
			flashSrc.Play ();

		}
	}

	//this is the function called by the left arrow button the make the ball move to the left
	public void MoveLeft()
	{
		//creat the vector with which the ball will be moved
		Vector3 moveLeftVector = new Vector3 (-mVal, 0f, 0f);

		//check if the ball is not in the left tube
		if (transform.position.x > -mVal) 
		{
			//play the flash particle systems according to the ball position
			if (transform.position.x == 0) 
			{
				flashMid.Play ();
			}
			if (transform.position.x == mVal) 
			{
				flashRight.Play ();
			}
			if (transform.position.x == -mVal) 
			{
				flashLeft.Play ();
			}

			//translate the ball and play the flash sound
			transform.Translate (moveLeftVector, Space.World);
			flashSrc.Play ();
		}
	}
		
	//this the function used to detect if the ball collided with the wrong color and call the explosion function in that case
	private void OnTriggerEnter(Collider col)
	{
        if (currentColor != col.gameObject.tag)
        {
            Explosion();
        }
	}

	//this is the function used to check if the ball has crossed a colored bar successfully, to call the game manager score function and call the function that changes the color
    private void OnTriggerExit(Collider other)
    {
        Coloring();
        GMScript.instance.Scored();
    }

    //this is the function used to change the color of the ball randomly
    void Coloring()
    {
        int colorToChooseNB = Random.Range(0, 3);
        coloring?.SetColorNum(colorToChooseNB);
        string colorToChoose = colorToChooseArray[colorToChooseNB];
        currentColor = colorToChoose;
    }

    public void Continue()
    {
        trail.Play();
    }

	//function called when the ball collides with a wrong color (playing particles and sound, hiding the ball and telling the game manager that the game is over
	private void Explosion()
	{
		explosionSrc.Play ();
		trail.Pause();
		explosionSys.Play ();
		GMScript.instance.GameOver();
	}

}

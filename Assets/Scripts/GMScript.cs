﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using PartaGames.Social;
using DG.Tweening;
using System;

public class GMScript : MonoBehaviour {

	public static GMScript instance;

	public bool gameOver=false, goFunctionCalled=false;

	public float cloudSpeed, cloudsDelay, tripleBarSpeed, tripleBarsDelay;

	public GameObject  gameCanvas,pauseButton,resumeButton,controlButtons,goCanvas;

	private int score,highScore;

	public Text scoreText, yourScoreText, highScoreText;
	public AudioSource scoreSound;

	private static readonly string TITLE = "Color Tubes";
	private static readonly string TEXT = "Can you beat my score in Color Tubes?!";


    public Action onContinue = delegate { }; 
	void Awake ()
	{
		//make sure there is only one game manager instance running
		if (instance == null)
			instance = this;
		else if (instance != this)
			Destroy (gameObject);
	}

	void Start()
	{
		//get the saved highscore from playerprefs file
		highScore = PlayerPrefs.GetInt ("highScore");
	}

	void Update()
	{
		
	}

	public void Scored()
	{
		//play the scoring sound, add one to the score and modify the UI score text	
		score++;
		scoreSound.Play ();
		scoreText.text = score.ToString ();
	}

    public void Continue()
    {
        gameCanvas.SetActive(true);
        goCanvas.SetActive(false);
       
        Time.timeScale = 1f;
        gameOver = false;

        onContinue.Invoke();

    }
    public void ShakeCam()
    {
       // DOTween.defaultTimeScaleIndependent = true; 
        Camera.main.transform.DOShakePosition(0.5f , strength: new Vector3(0.005f, 0.005f , 0f)).SetUpdate(true);
      
    }

    public void GameOver()
	{

        AdShowManager.Instance.Show("lose"); 
		//play the camera shaking animation, disable poolers and the game canvas and prepare text for game over canvas
        ShakeCam();
        gameOver = true;
        gameCanvas.SetActive (false);
		StartCoroutine (ShowGOCanvas ());
        goCanvas.SetActive(true);
        yourScoreText.text = score.ToString ();
		highScoreText.text = "Best "+highScore.ToString ();
        Time.timeScale = 0f;

		if (score > highScore) 
		{
			//if the current score is greater than highscore let's update the saved highscore
			PlayerPrefs.SetInt ("highScore", score);
		}
	}

	//pause button function
	public void Pause()
	{
		Time.timeScale = 0f;
		pauseButton.SetActive(false);
		controlButtons.SetActive (false);
		resumeButton.SetActive(true);
	}

	//resume button function
	public void Resume()
	{
		Time.timeScale = 1f;
		pauseButton.SetActive(true);
		controlButtons.SetActive (true);
		resumeButton.SetActive(false);
	}

	//retry button function
	public void Retry()
	{
        Time.timeScale = 1f; 
		SceneManager.LoadScene ("GameScene");
	}

	//home button function
	public void GoHome()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene ("MainMenu");
	}
		
	//couroutine that enable the game over canvas after 1 second from calling GameOver()
	IEnumerator ShowGOCanvas()
	{
		yield return new WaitForSecondsRealtime (1f);

		goCanvas.SetActive (true);
	}

	//this is the function used to take a screenshot (called by ShareTextAndImage())
	private string TakeScreenshot()
	{
		string fileName = "CTscreenshot.png";
		string filePath = Application.persistentDataPath + "/"+fileName;
		ScreenCapture.CaptureScreenshot (fileName);

		return filePath;
	}


	//share button Function
	public void ShareTextAndImage() 
	{
		NativeShare.Share(TITLE, TEXT, TakeScreenshot());
	}
}

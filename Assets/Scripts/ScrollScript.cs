﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScrollScript : MonoBehaviour {

	private Rigidbody2D rb2d;
	private Scene currentScene;

	void Start()
	{
		//get the rigidbody2d component
		rb2d = gameObject.GetComponent<Rigidbody2D> ();
		//get the current scene (we use that to set the velocity according to the name of the scene)
		currentScene = SceneManager.GetActiveScene ();
	}


	//this is the function used to make clouds and tripleBars move on the Y axis, and that destroyes them after they leave screen
	void FixedUpdate()
	{
		if(this.CompareTag("cloud"))
		{
			if (currentScene.name == "GameScene") 
			{
				rb2d.velocity = new Vector2 (0, -0.03f);
			} 
			else if (currentScene.name == "MainMenu")
			{
				rb2d.velocity = new Vector2 (0, -0.03f);
			}
		}

		if(currentScene.name=="GameScene")
		{
			if (GMScript.instance.gameOver == true ) 
			{
				rb2d.velocity = Vector2.zero;
			}
		}

		if (transform.position.y <= 0.3f) 
		{
			Destroy (gameObject);
		}
	}
}

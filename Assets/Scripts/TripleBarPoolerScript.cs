﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleBarPoolerScript : MonoBehaviour {

	public GameObject[] triplebars;
	private float timer;

	void Start()
	{
		//get the value of the timer from the game manager
		timer = GMScript.instance.tripleBarsDelay;
	}

	//this is the function that spawns new triplebar obstacles according to the timer
	void Update()
	{
		timer -= Time.deltaTime;

		if (timer <= 0) 
		{
			int tripleBarNb = Random.Range (0, 6);
			Instantiate (triplebars [tripleBarNb], new Vector3 (0, 7f, 0f), Quaternion.identity);
			timer = GMScript.instance.tripleBarsDelay;
		}
	}

}

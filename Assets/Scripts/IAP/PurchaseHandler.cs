﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using System;
using System.Linq;  

public class PurchaseHandler : MonoBehaviour {


    public enum IapType
    {
        none = 0 , 
     
        coin_1=2, 
        coin_2=4, 
        coin_3=6,
               noads = 8,
        gold = 10,
       

    }
    private void Awake()
    {


        PurchaseManager.OnPurchaseNonConsumable += PurchaseManager_OnPurchaseNonConsumable;
        PurchaseManager.OnPurchaseConsumable += PurchaseManager_OnPurchaseConsumable;


    }
    private void OnDestroy()
    {
        PurchaseManager.OnPurchaseNonConsumable -= PurchaseManager_OnPurchaseNonConsumable;
        PurchaseManager.OnPurchaseConsumable -= PurchaseManager_OnPurchaseConsumable;
    }

    void PurchaseManager_OnPurchaseNonConsumable(PurchaseEventArgs args)
    {
        IapType type = GettingIapType(args);
       
        switch (type)
        {
           
            case IapType.noads:
                AdShowManager.Instance.BuyDisableAds();
                break;
            default:

                Debug.LogWarning("Select Non consumable purshase? with do not have definition ", this); 
                break;
        }
    }

    void PurchaseManager_OnPurchaseConsumable(PurchaseEventArgs args)
    {

        IapType type = GettingIapType(args);
        int coinsPurchased = 0;
        switch (type)
        {
            case IapType.coin_1:
                coinsPurchased = 60;
                break;
            case IapType.coin_2:
                coinsPurchased = 500;
                break;
            case IapType.coin_3:
                coinsPurchased = 3500;
                break;
            case IapType.noads:
                AdShowManager.Instance.BuyDisableAds(); 
                break; 
            default: break; 
        }
        Debug.Log("PurchaseManager_OnPurchaseConsumable, coins purchased " + coinsPurchased);
      

    }
    IapType GettingIapType(PurchaseEventArgs args)
    {

        string targetType = args.purchasedProduct.definition.id;
        IapType type;

        if (!Enum.TryParse(targetType, out type))
        {
            Debug.LogWarning($"Purchasing mistake ? do not find {targetType} in avaluable values  ", this);
            return IapType.none;
            
        }
        if (type == IapType.none) Debug.LogWarning("Purchase have default value " + IapType.none.ToString(), this);
        return type; 
        
    }





}

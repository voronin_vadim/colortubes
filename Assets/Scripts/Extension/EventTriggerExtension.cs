﻿
using UnityEngine.EventSystems;
using System;
using System.Linq; 

public static class EventTriggerExtension{

    public static void AddListener(this EventTrigger eventTr, EventTriggerType type, Action listener)
    {
        EventTrigger.Entry entry = null; 
        entry =  eventTr.triggers.FirstOrDefault(p => p.eventID == type);
        // entry = entry ?? new EventTrigger.Entry();
        if (entry == null)
        {
            entry = new EventTrigger.Entry();
            eventTr.triggers.Add(entry); 
        }
        entry.eventID = type;
        entry.callback.AddListener((p) => { listener.Invoke(); }); 
        

    }
}

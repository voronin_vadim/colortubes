﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; 

public static  class IEnurableExtension  {

	
    public static  IEnumerable<TResult> JoinByPosition<Ta, Tb, TResult>(this IEnumerable<Ta> qa, IEnumerable<Tb> qb, Func<Ta, Tb, TResult> makePair)
    {
        using (var ia = qa.GetEnumerator())
        using (var ib = qb.GetEnumerator())
            while (ia.MoveNext() && ib.MoveNext())
                yield return makePair(ia.Current, ib.Current);
    }

    ///<summary>Finds the index of the first item matching an expression in an enumerable.</summary>
    ///<param name="items">The enumerable to search.</param>
    ///<param name="predicate">The expression to test the items against.</param>
    ///<returns>The index of the first matching item, or -1 if no items match.</returns>
    public static int FindIndex<T>(this IEnumerable<T> items, Func<T, bool> predicate)
    {
        if (items == null) throw new ArgumentNullException("items");
        if (predicate == null) throw new ArgumentNullException("predicate");

        int retVal = 0;
        foreach (var item in items)
        {
            if (predicate(item)) return retVal;
            retVal++;
        }
        return -1;


    }
}



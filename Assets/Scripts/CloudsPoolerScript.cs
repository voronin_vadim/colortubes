﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CloudsPoolerScript : MonoBehaviour {

	public GameObject[] clouds;
	private float timer;
	private Scene currentScene;

	void Awake()
	{
		//get the current scene and store it in currentScene;
		currentScene = SceneManager.GetActiveScene ();
	}
	void Start()
	{
		//set the value of the timer according to the current scene name
		if (currentScene.name == "GameScene") 
		{
            timer = 1f;// GMScript.instance.cloudsDelay;
		}
		if (currentScene.name == "MainMenu") 
		{
			timer = 1f;
		}
	}

	//this the function that spawns new clouds according to the timer
	void FixedUpdate()
	{
		
		timer -= Time.deltaTime;

		if (timer <= 0) 
		{
			int cloudNb = Random.Range (0, 4);
			float posX = Random.Range (-0.04f, 0.04f);
			Instantiate (clouds [cloudNb], new Vector3 (posX, 0.65f, 0f), Quaternion.identity);

			if (currentScene.name == "GameScene") 
			{
				timer = GMScript.instance.cloudsDelay;
			}
			if (currentScene.name == "MainMenu") 
			{
				timer = 4f;
			}
		}
	}
}

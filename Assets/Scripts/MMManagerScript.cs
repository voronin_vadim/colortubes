﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using GameAnalyticsSDK;
using Facebook; 

public class MMManagerScript : MonoBehaviour {

	private float volume;

	public GameObject muteButton, unMuteButton;
    private void Awake()
    {
        GameAnalytics.Initialize();
        Facebook.Unity.FB.Init();
    }
    void Start()
	{
		//get the saved volume from playerprefs file
		volume = PlayerPrefs.GetFloat ("volume", 1);

		//hide or show mute and unmute button according to volume
		if (volume == 1f) 
		{
			muteButton.SetActive (true);
			unMuteButton.SetActive (false);
		}

		if (volume == 0f) 
		{
			muteButton.SetActive (false);
			unMuteButton.SetActive (true);
		}


	}

	//play button function
	public void Play()
	{
		SceneManager.LoadScene ("GameScene");
	}

	//quit button function
	public void Quit()
	{
		Application.Quit ();
	}

	//mute button function
	public void Mute()
	{
		muteButton.SetActive (false);
		unMuteButton.SetActive (true);
		PlayerPrefs.SetFloat ("volume", 0f);
	}

	//unmute button function
	public void UnMute()
	{
		muteButton.SetActive (true);
		unMuteButton.SetActive (false);
		PlayerPrefs.SetFloat ("volume", 1f);
	}
}

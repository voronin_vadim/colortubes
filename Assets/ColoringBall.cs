﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoringBall : MonoBehaviour {

    public List<Material> material;
    MeshRenderer renderer;
    public ParticleSystem trail;
    public List<Color> trailColors; 

     public void SetColorNum(int num)
    {
        if (material == null || material.Count == 0) { 
            Debug.LogError("Do not set coloring matrial" , this);
            return;
        }
        if (num >= material.Count)
        {
            Debug.LogWarning("Setin to match number" , this);
            num = 0; 
        }
        renderer.sharedMaterial = material[num];
        trail.startColor = trailColors[num]; 


    }

    private void Awake()
    {
        renderer = GetComponent<MeshRenderer>(); 
    }
}
